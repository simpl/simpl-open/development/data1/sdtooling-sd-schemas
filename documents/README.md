# Tools for Self-Description

We divide the toolchain into end-user tools. The toolchain supports CI/CD Pipeline. End user tools are for users to create and manage self-descriptions. We support the following end user tools:

- Creation Wizard

## End User Tool

### Creation Wizard

The documentation of the creation wizard can be found [here](https://gitlab.com/gaia-x/data-infrastructure-federation-services/self-description-tooling/sd-creation-wizard/sd-creation-wizard-frontend/-/blob/main/README.md)

### Ontology / Shape - Generation

```shell
python cli.py ontology-generation --srcFile ./pathFile.csv --dstPathOnto ./yaml2ontology/ --dstPathSchema ./yaml2shape -e simpl
```

This command allows you to generate the ontology and shapes files (NOTE: The contract directory present in the yaml directory will be ignored for ontology generation)

With each commit, the latest version of the ontology is computed.

In more detail, for each prefix (aka. ecosystem) the script generates one ontology file (e.g trusted-cloud.ttl) that describes
all classes associated with this prefix (e.g trustedCloudProvider.yaml', 'participant.yaml', 'provider.yaml').

- Each ontology file starts with a list of prefixes that are used within the ontology file.
- Each ontology file includes a definition of the associated gax-prefix. This information is not included in the *.yaml files.
  Thus, we utilize other files to define this definition.
- Each gax-prefix definition has a property dct:modified described as a xsd:dateTimeStamp, which is set to the execution time of the script.
- Each ontology file includes a definition of classes (owl:Class). Each owl:Class is defined by one *.yaml file.
- Each definition of a class  includes the class definition itself and a definition of its associated DatatypeProperties (owl:DatatypeProperty).

These SHACL shapes define constraints on certain classes which are used to validate the correctness of [Self Descriptions](https://gaia-x.gitlab.io/technical-committee/architecture-document//self-description/) in the Gaia-X data space. The creation of the SHACL files is done by a [python script](https://gitlab.com/gaia-x/gaia-x-community/gaia-x-self-descriptions/-/blob/master/toolchain/yaml2shacl.py)
which maps attributes defined in the YAML files to corresponding SHACL properties as defined in the [SHACL Documentation](https://www.w3.org/TR/shacl/). If no corresponding SHACL property exists, properties of other ontologies are used. The table below gives an overview of the currently supported mappings.

| YAML attribute       | SHACL command               |
|----------------------|-----------------------------|
| title                | sh:name                     |
| description          | sh:description              |
| exampleValues        | skos:example                |
| minValue             | sh:minInclusive             |
| maxValue             | sh:maxInclusive             |
| valueIn              | sh:in                       |
| length               | sh:minLength / sh:maxLength |
| cardinality          | sh:minCount / sh:maxCount   |
| pattern              | sh:pattern                  |
| flags                | sh:flags                    |
| configure            | simpl:configure             |

The resulting SHACL shapes are released as [GitLab Package](https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/packages). For every official Gaia-X Trust Framework release, a new package is released.

The configure property is a list of string and accept: 
- hiddenInFrontend (for hidden the field in Self Description UI)
- useForAdvancedSearch (for show the field in Search UI)
- requiredOnFrontendOnly (for set required field only on Sd Tool front-end)

configure: [ 'hiddenInFrontend', 'useForAdvancedSearch', 'requiredOnFrontendOnly' ]

### Merge

If the shapes file to be uploaded to the catalog are multiple, to correctly upload the shape file into the catalog, you need to merge the generated shape files to create a single one.

```shell
python merge_shape.py merged-generation yaml2shape/service-offering/application-offeringShape.ttl yaml2shape/service-offering/data-offeringShape.ttl yaml2shape/service-offering/infrastructure-offeringShape.ttl
```

The command creates the merged-shapes.ttl file

## Upload Schema and Ontology in Federated Catalog

### Files to use

- Ontology (present in yaml2ontology directory)
```shell
"simpl_ontology_generated.ttl"
```

- Schema
```shell
"merged-shapes.ttl" 
```

### Upload a new Schema/Ontology to the Federated Catalog
If it does not exist, you need to create the schema/ontology using this API. Otherwise, you need to use the next step to update a schema/ontology.
Update

```shell
[POST] https://fc-server.dev.simpl-europe.eu/schemas
```

### Update an existing Schema/Ontology in the Federated Catalog
If it already exists, you need to update the schema/ontology using this API. Otherwise, you need to use the previous point to create a schema/ontology.

```shell
[PUT] https://fc-server.dev.simpl-europe.eu/schemas
```
### Update an existing Schema/Ontology in the Federated Catalog
If it already exists, you need to update the schema/ontology using this API. Otherwise, you need to use the previous point to create a schema/ontology.

```shell
[PUT] https://fc-server.dev.simpl-europe.eu/schemas
```

### View the Ontologies and Schemas present in the Federated Catalog
```shell
[GET] https://fc-server.dev.simpl-europe.eu/schemas
```