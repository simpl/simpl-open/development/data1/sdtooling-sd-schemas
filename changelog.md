# Changelog

All notable changes to this project will be documented in this file.

## [1.4.0] - 2024-12-20 (Sprint 9)

### Added

- Added property "configure".
  This property accepts the following list of parameters:
  - hiddenInFrontend (to hide the field in the SD Tool UI)
  - useForAdvancedSearch (to show the field in the Catalogue Search UI)
  - requiredOnFrontendOnly (to set the field as mandatory only in the SD Tool UI)
    ```
    Example:
    configure: [ 'hiddenInFrontend', 'useForAdvancedSearch' ]
    ```

### Changed
Updated files:
- merged-shapes.ttl
- yaml/simpl/data-types/application-properties.yaml
- yaml/simpl/data-types/billing-schema.yaml
- yaml/simpl/data-types/contract-template.yaml
- yaml/simpl/data-types/data-properties.yaml
- yaml/simpl/data-types/edc-connector.yaml
- yaml/simpl/data-types/edc-registration.yaml
- yaml/simpl/data-types/general-service-properties.yaml
- yaml/simpl/data-types/infrastructure-properties.yaml
- yaml/simpl/data-types/offering-price.yaml
- yaml/simpl/data-types/provider-information.yaml
- yaml/simpl/data-types/service-policy.yaml
- yaml/simpl/data-types/sla-agreements.yaml
- yaml2ontology/simpl_ontology_generated.ttl
- yaml/simpl/service-offering/service-offering.yaml
- yaml2shacl.py
- yaml2shape/service-offering/application-offeringShape.ttl
- yaml2shape/service-offering/data-offeringShape.ttl
- yaml2shape/service-offering/infrastructure-offeringShape.ttl
