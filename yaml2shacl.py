"""
Module Name: Yaml to Shacl.

This module provides utility functions for converting yaml to shacl files.
It includes functions for loading, processing, and saving files.

"""

import argparse
import yaml
import re
import os
import fnmatch
import rdflib
import logging
import colorlog

from rdflib import Graph
from os.path import exists

# configure logger
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
formatter = colorlog.ColoredFormatter(
    '%(asctime)s - %(log_color)s%(levelname)s - %(message)s%(reset)s',
    datefmt='%Y-%m-%d %H:%M:%S',
    log_colors={
        # 'DEBUG': 'blue',
        # 'INFO': 'green',
        'WARNING': 'yellow',
        'ERROR': 'red',
        'CRITICAL': 'bold_red',
    }
)
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
logger.addHandler(stream_handler)


def parse_yaml(path):
    """
    Parse a YAML file and return its contents as a dictionary.

    Args:
        path (str): Path to the YAML file.

    Returns:
        Dict: Parsed data from the YAML file.

    Raises:
        yaml.YAMLError: If the YAML file contains a syntax error.
    """
    with open(path, "r") as stream:
        try:
            data = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            logger.error("Parse yaml error %s", str(exc))
            raise exc
    return data


def get_name(title):
    """
    Split a camel-case string into separate words and return the result in lowercase.

    Args:
        title (str): The camel-case string to process.

    Returns:
        str: A space-separated, lowercase version of the input string.
    """
    words = re.split('(?=[A-Z])', title)
    name = ''
    for word in words:
        name += word + ' '
    name = name[:-1]
    return name.lower()


def add_super_classes(class_names, ignore_classes=None, max_depth=10, current_depth=0):
    """
    For a given list of classes, recursively find and return their yaml sources.

    Args:
        class_names (list): List of class names to process.
        ignore_classes (set, optional): Set of classes to ignore. Defaults to None.
        max_depth (int, optional): Maximum recursion depth. Defaults to 10.
        current_depth (int, optional): Current recursion depth. Defaults to 0.

    Returns:
        list: List of YAML data sources for the given classes.
    """
    if current_depth >= max_depth:
        logger.warning("Max recursion depth reached: %d", max_depth)
        return []

    datas = []
    files = []
    if ignore_classes is None:
        ignore_classes = set()

    super_classes = set(class_names) - set(ignore_classes)
    for class_name in super_classes:
        ecosystem = class_name.split(':')[0]
        class_name = class_name.split(':')[1]
        if ecosystem in ['gax-core', 'gax-trust-framework', 'trusted-cloud', 'simpl']:  # ENG add simpl
            files = get_yaml_files_from_folder(get_yaml_location(ecosystem), ecosystem, True)

        for filename in files:
            if class_name.lower() in filename.lower().replace('-', ''):
                data = parse_yaml(filename)
                if is_correct_yaml_for_class(data, class_name):
                    ignore_classes.add(class_name)
                    datas.append(data)
                    new_super_class_names = get_super_classes_from_yaml(data)
                    datas += add_super_classes(
                        new_super_class_names,
                        ignore_classes.union(set(class_names)),
                        max_depth,
                        current_depth + 1
                    )
    return datas


def append_object_shapes(object_references, file, ecosystem, visited=None):
    """
    Recursively append shapes for referenced objects to the output file.

    Args:
        object_references (set): Set of object references to process.
        file (file object): The output file where shapes will be written.
        ecosystem (str): The ecosystem to which the objects belong.
        visited (set, optional): Set of already visited object references. Defaults to None.
    """
    if visited is None:
        visited = set()
    elif not isinstance(visited, set):
        visited = set(visited)

    object_references = set(object_references) - visited
    if exists(get_validation_yaml_location(ecosystem)):
        files = get_yaml_files_from_folder(get_validation_yaml_location(ecosystem), ecosystem, False)
        for objectReference in object_references:
            prefix, suffix = objectReference.split(":")
            for filename in files:
                if suffix.lower() in filename.lower().replace('-', ''):
                    data = parse_yaml(filename)
                    if is_correct_yaml_for_object(data, objectReference):
                        names, new_references = write_shape_to_file(data, file, ecosystem)
                        visited.add(objectReference)
                        unique_objects = set(new_references) - visited
                        append_object_shapes(unique_objects, file, ecosystem, visited)


def get_super_classes_from_yaml(data):
    """
    Extract and return the list of superclasses from a YAML data structure.

    Args:
        data (dict): Parsed YAML data containing class definitions.

    Returns:
        list: A list of superclass names referenced in the YAML data.
    """
    super_class_names = []
    for key in [*data.keys()]:
        super_class_names += data[key]["subClassOf"]
    return super_class_names


def is_correct_yaml_for_object(yaml_data, object_reference):
    """
    Verify if the provided YAML data is suitable for generating a shape for the given object reference.

    Args:
        yaml_data (dict): Parsed YAML data containing object definitions.
        object_reference (str): The object reference in the format 'prefix:suffix'.

    Returns:
        bool: True if the YAML data matches the object reference, False otherwise.
    """
    prefix, suffix = object_reference.split(":")
    yaml_name = [*yaml_data.keys()][0]
    yaml_prefix = yaml_data[yaml_name]["prefix"]
    if yaml_name == suffix and yaml_prefix == prefix:
        return True
    return False


def is_correct_yaml_for_class(yaml_data, class_name):
    """
    Check if the given YAML data contains information for the specified class.

    Args:
        yaml_data (dict): Parsed YAML data containing class definitions.
        class_name (str): The name of the class to check.

    Returns:
        bool: True if the YAML data contains the class definition, False otherwise.
    """
    yaml_name = [*yaml_data.keys()][0]
    if yaml_name == class_name:
        return True
    return False


def get_shape_name_for_object(name, ecosystem):
    """
    For a given object reference name and ecosystem, find the matching YAML definition and construct the shape name.

    Args:
        name (str): The object reference name.
        ecosystem (str): The ecosystem in which the object resides.

    Returns:
        str or None: The shape name in the format "gax-validation:{object_name}Shape" if a matching shape is found,
                     or None if no matching shape is found.
    """
    if exists(get_validation_yaml_location(ecosystem)):
        yaml_location = get_validation_yaml_location(ecosystem)
        files = get_yaml_files_from_folder(yaml_location, ecosystem, True)
        for file in files:
            data = parse_yaml(file)
            if is_correct_yaml_for_object(data, name):
                return "gax-validation:%sShape" % ([*data.keys()][0])
    return None


def write_shape_to_file(src_data, shacl_file, ecosystem):
    """
    Recursively writes shapes for the referenced objects and their attributes from a YAML source to an SHACL file.

    Args:
        src_data (dict): The source data containing object definitions.
        shacl_file (file object): The output SHACL file where shapes will be written.
        ecosystem (str): The ecosystem to which the objects belong.

    Returns:
        tuple: A tuple containing:
            - names (list): A list of names for the defined shapes.
            - object_references (list): A list of object references.
    """
    object_references = []
    names = []
    # Create shapes for each class defined in the YAML file
    for key in [*src_data.keys()]:
        prefix = src_data[key]["prefix"]
        super_class_names = src_data[key]["subClassOf"]
        super_classes_data_list = add_super_classes(super_class_names)
        data_list = [src_data] + super_classes_data_list
        # Add sh:targetClass for the shape
        line = "\ngax-validation:%sShape\n\ta sh:NodeShape ;\n\tsh:targetClass %s:%s ;\n" % (
            key, prefix, key)
        names.append("%s:%s" % (prefix, key))
        shacl_file.write(line)
        for dataSet in data_list:
            key = [*dataSet.keys()][0]
            # Add constraints depending on the attributes defined in the YAML file
            order = 1
            for attribute in dataSet[key]["attributes"]:

                # Add sh:path constraint
                line = "\tsh:property [ sh:path %s:%s ;\n" % (attribute["prefix"], attribute["title"])
                shacl_file.write(line)

                # Add sh:name
                line = "\t\t\t\t  sh:name \"%s\" ;\n" % get_name(attribute["title"])
                shacl_file.write(line)

                # Add sh:description    
                line = "\t\t\t\t  sh:description \"%s\";\n" % attribute["description"].replace('\"', '\'')  # ENG
                shacl_file.write(line)  # ENG

                # ENG
                # Add sh:rule
                if [*attribute.keys()].__contains__("rule"):
                    line = "\t\t\t\t  sh:rule \"%s\" ;\n" % get_name(attribute["rule"])
                    shacl_file.write(line)
                # ENG
                # Add simpl:hiddenInFrontend
                if [*attribute.keys()].__contains__("hiddenInFrontend"):
                    line = "\t\t\t\t  simpl:hiddenInFrontend \"%s\" ;\n" % get_name(attribute["hiddenInFrontend"])
                    shacl_file.write(line)
                # ENG
                # Add simpl:useForAdvancedSearch
                if [*attribute.keys()].__contains__("useForAdvancedSearch"):
                    line = "\t\t\t\t  simpl:useForAdvancedSearch \"%s\" ;\n" % get_name(
                        attribute["useForAdvancedSearch"])
                    shacl_file.write(line)

                # ENG
                # Add simpl:configure
                if list(attribute.keys()).__contains__("configure"):
                    line = "\t\t\t\t  simpl:configure (%s) ;\n" % str(attribute["configure"]).replace('[', '').replace(
                        ']', '').replace(',', '').replace('\'', '\"')
                    shacl_file.write(line)

                # ENG (old code for manage the description as list, check it first of uncomment)
                # for char in attribute.get("description", ""):
                #    try:
                #        value = attribute["description"][char]                        
                #        line = "\t\t\t\t  sh:description \"%s\"@%s ;\n" % (value.replace('\"', '\''), char)
                #        shaclFile.write(line)
                #    except TypeError:
                #        logging.basicConfig(level=logging.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')
                #        logging.debug(char)
                #        #line = "\t\t\t\t  sh:description \"%s\";\n" % attribute["description"]
                #        #shaclFile.write(line)
                #        pass

                # Add skos:example
                if key == "Address":
                    line = "\t\t\t\t  skos:example \"%s\" ;\n" % str((attribute["exampleValues"])).replace('[', '')
                else:
                    line = "\t\t\t\t  skos:example \"%s\" ;\n" % str((attribute["exampleValues"])).replace('[',
                                                                                                           '').replace(
                        ']', '').replace('\"', '\'')
                shacl_file.write(line)

                # Add sh:order
                line = "\t\t\t\t  sh:order %s ;\n" % order
                shacl_file.write(line)
                order += 1

                # Add sh:minInclusive
                if [*attribute.keys()].__contains__("minValue"):
                    line = "\t\t\t\t  sh:minInclusive %s ;\n" % float(attribute["minValue"])
                    shacl_file.write(line)

                # Add sh:maxInclusive
                if [*attribute.keys()].__contains__("maxValue"):
                    line = "\t\t\t\t  sh:maxInclusive %s ;\n" % float(attribute["maxValue"])
                    shacl_file.write(line)

                # Add sh:in
                if list(attribute.keys()).__contains__("valueIn"):
                    line = "\t\t\t\t  sh:in (%s) ;\n" % str(attribute["valueIn"]).replace('[', '').replace(']',
                                                                                                           '').replace(
                        ',', '').replace('\'', '\"')
                    shacl_file.write(line)

                # Add sh:minCount / sh:maxCount constraints if defined in the YAML file
                if [*attribute.keys()].__contains__("length"):
                    min_length = attribute["length"].split('..')[0]
                    max_length = attribute["length"].split('..')[1]
                    if min_length != "0":
                        line = "\t\t\t\t  sh:minLength %s ;\n" % (min_length)
                        shacl_file.write(line)
                    if max_length != "*":
                        line = "\t\t\t\t  sh:maxLength %s ;\n" % (max_length)
                        shacl_file.write(line)

                # Add sh:minLength / sh:maxLength constraints if defined in the YAML file
                if [*attribute.keys()].__contains__("cardinality"):
                    min_count = attribute["cardinality"].split('..')[0]
                    max_count = attribute["cardinality"].split('..')[1]
                    if min_count != "0":
                        line = "\t\t\t\t  sh:minCount %s ;\n" % (min_count)
                        shacl_file.write(line)
                    if max_count != "*":
                        line = "\t\t\t\t  sh:maxCount %s ;\n" % (max_count)
                        shacl_file.write(line)

                # Add sh:pattern
                if [*attribute.keys()].__contains__("pattern"):
                    # line = "\t\t\t\t  sh:pattern \"%s\" ;\n" % getName(attribute["pattern"])
                    line = "\t\t\t\t  sh:pattern \"%s\" ;\n" % attribute["pattern"]
                    shacl_file.write(line)

                # Add sh:flags
                if [*attribute.keys()].__contains__("flags"):
                    line = "\t\t\t\t  sh:flags \"%s\" ;\n" % get_name(attribute["flags"])
                    shacl_file.write(line)

                # Add either sh:datatype or sh:class constraint depending on the defined range of the property
                if attribute["dataType"].__contains__("xsd"):
                    line = "\t\t\t\t  sh:datatype %s ] ;\n" % attribute["dataType"]
                    shacl_file.write(line)
                elif attribute["dataType"].__contains__("meaningfulString"):
                    line = "\t\t\t\t  sh:datatype xsd:string ] ;\n"
                    shacl_file.write(line)
                elif [*attribute.keys()].__contains__("dataType"):
                    shape_name = get_shape_name_for_object(attribute["dataType"], ecosystem)
                    if shape_name:
                        object_references.append(attribute["dataType"])
                        line = "\t\t\t\t  sh:node %s ] ;\n" % shape_name
                    elif attribute["dataType"] in simple_data_types:
                        line = "\t\t\t\t  sh:class %s ] ;\n" % attribute["dataType"]
                    else:
                        line = "\t\t\t\t  sh:nodeKind sh:IRI ] ;\n"
                    shacl_file.write(line)

        # Add dot to close the shape
        line = ".\n"
        shacl_file.write(line)
        return names, object_references


def write_shacl_file(prefixes, data, output, ecosystem):
    """
    Write the SHACL file based on the given prefixes and data, and serialize it with corrections.

    Args:
        prefixes (dict): The dictionary containing the prefixes to be added to the SHACL file.
        data (dict): The YAML data used to generate the shapes in the SHACL file.
        output (str): The file path where the SHACL file will be written.
        ecosystem (str): The ecosystem to which the objects belong.

    Returns:
        None
    """
    # Create empty SHACL file
    shacl_file = open(output, "w")
    logger.info(output)

    if prefixes:
        # Add SHACL and gax-validation prefix
        line = "@prefix sh: <http://www.w3.org/ns/shacl#> .\n"
        shacl_file.write(line)
        line = "@prefix gax-validation:  <http://w3id.org/gaia-x/validation#> .\n\n"
        shacl_file.write(line)

        # Add all prefixes defined in the YAML file
        for prefix in prefixes["Prefixes"]:
            line = "@prefix %s: <%s> .\n" % (prefix["name"], prefix["value"])
            shacl_file.write(line)

    names, object_references = write_shape_to_file(data, shacl_file, ecosystem)
    append_object_shapes(object_references, shacl_file, ecosystem, names)
    shacl_file.close()

    # QUICK FIX Solve this properly for every possible shape
    g = Graph()
    g.parse(output, format='turtle')
    i = 0
    for s in g.subjects(rdflib.term.URIRef('http://www.w3.org/ns/shacl#path'),
                        rdflib.term.URIRef('http://w3id.org/gaia-x/gax-trust-framework#value')):
        i += 1
        if i > 1:
            g.remove((s, None, None))
            g.remove((None, rdflib.term.URIRef('http://www.w3.org/ns/shacl#property'), s))
    i = 0
    for s in g.subjects(rdflib.term.URIRef('http://www.w3.org/ns/shacl#path'),
                        rdflib.term.URIRef('http://w3id.org/gaia-x/gax-trust-framework#unit')):
        i += 1
        if i > 1:
            g.remove((s, None, None))
            g.remove((None, rdflib.term.URIRef('http://www.w3.org/ns/shacl#property'), s))
    g.serialize(output, format='turtle')

    shacl_file = open(output, "rt")
    data_of_shacl = shacl_file.read()
    # data_of_shacl = data_of_shacl.replace('http://w3id.org/gaia-x', '{{BASE_URI}}') #ENG
    shacl_file.close()
    shacl_file = open(output, "wt")
    shacl_file.write(data_of_shacl)
    shacl_file.close()


def get_yaml_files_from_folder(path, ecosystem, include_subdirectories=True):
    """
    Recursively retrieve all YAML files from a given folder and its subdirectories, excluding specific files.

    Args:
        path (str): The directory path to start the search for YAML files.
        ecosystem (str): The ecosystem to which the files belong (used for filtering specific paths).
        include_subdirectories (bool): Flag to include files from subdirectories. Defaults to True.

    Returns:
        list: A list of file paths to the YAML files found.
    """
    files = fnmatch.filter(os.listdir(path), "*.yaml")
    files = [os.path.join(path, f).replace("\\", "/") for f in files if
             (f != "dataTypeAbbreviation.yaml" and f != "prefixes.yaml")]
    if include_subdirectories:
        subdirectories = [x[0] for x in os.walk(path) if not x[0] == path]
        for directory in subdirectories:
            # if directory != "../single-point-of-truth/yaml/to-be-integrated" and directory != "../single-point-of-truth/yaml/%s/data-types" % ecosystem:
            if directory != "../single-point-of-truth/yaml/to-be-integrated" and directory != "../single-point-of-truth/yaml/%s/data-types" % ecosystem:
                files = files + get_yaml_files_from_folder(directory, ecosystem, True)
    return files


def get_output_file_location_for_yaml_filename(path, output_path):
    """
    Construct the output file location for a given YAML file, based on the provided path and output directory.

    Args:
        path (str): The full path to the input YAML file.
        output_path (str): The base directory for the output files.

    Returns:
        str: The full path to the output file (with the .ttl extension).
    """
    filename = os.path.basename(path)
    dir_yaml_name = os.path.basename(output_path)
    if not destPath:
        return output_path + "/" + filename.split(".")[0] + "Shape.ttl"
    else:
        if not os.path.isdir(destPath + "/" + dir_yaml_name):
            os.mkdir(destPath + "/" + dir_yaml_name)
        return destPath + "/" + os.path.basename(output_path) + "/" + filename.split(".")[0] + "Shape.ttl"


def get_validation_yaml_location(ecosystem):
    """
    Return the location of the validation YAML files for a given ecosystem.

    Args:
        ecosystem (str): The name of the ecosystem.

    Returns:
        str: The directory path to the validation YAML files for the specified ecosystem.
    """
    # return "../single-point-of-truth/yaml/%s/data-types/" % ecosystem
    # return "yaml/%s/data-types/" % ecosystem
    validation_path = os.getenv('VALIDATION_PATH', 'yaml/%s/data-types/' % ecosystem)
    return validation_path


def get_yaml_location(ecosystem: str):
    """
    Return the location of the YAML files for a given ecosystem.

    Args:
        ecosystem (str): The name of the ecosystem.

    Returns:
        str: The directory path to the YAML files for the specified ecosystem.
    """
    # return "../single-point-of-truth/yaml/%s/" % ecosystem
    return "yaml/%s/" % ecosystem


def iterate_folder(path, ecosystem):
    """
    Iterate through a folder to process YAML files, generates SHACL files, and saves them to an output location.

    Args:
        path (str): The directory path containing the YAML files.
        ecosystem (str): The ecosystem associated with the YAML files.
    """
    files = get_yaml_files_from_folder(path, ecosystem)

    for file in files:

        output_path = str(os.path.dirname(file).replace('preprocessed_yaml/', 'yaml/'))

        if not exists(output_path):
            os.mkdir(output_path)
        output = get_output_file_location_for_yaml_filename(file, output_path)
        data = parse_yaml(file)
        write_shacl_file(prefixes, data, output, ecosystem)


if __name__ == "__main__":

    # ecosystems = sys.argv[1:]

    # Setup parser for sd-attribute command
    parser = argparse.ArgumentParser(
        description='generates shape files')

    sub_parser = parser.add_subparsers(title="Output to be generated", dest="output")

    y2j_parser = sub_parser.add_parser("shape-generation",
                                       help="Creates a turtle file for each prefix in the single source of truth, which defines the schema")

    y2j_parser.add_argument('--dstPath', type=str, required=True,
                            help='Path to file specifying where to generating specific files')

    y2j_parser.add_argument('-e', '--ecosystem', action='append', required=True,
                            help='Select ecosystem, if ecosystem depends on other ecosystem please specify as list e.g ( --ecosystem gax-core --ecosystem trusted-cloud')

    args = parser.parse_args()

    destPath = args.dstPath

    ecosystems: list = args.ecosystem

    if args.output == "shape-generation":
        logger.info("Starting to generate schema files (*.ttl) ===>")

        for item in ecosystems:
            path = "yaml/%s/" % item
            if not exists(path):
                os.mkdir(path)
            dataTypeAbbreviation = 'yaml/validation/%s/dataTypeAbbreviation.yaml' % item
            if os.stat(dataTypeAbbreviation).st_size > 0:
                simple_data_types = list(parse_yaml(dataTypeAbbreviation).keys())
            else:
                simple_data_types = []
            prefixFile = "yaml/validation/%s/prefixes.yaml" % item
            prefixes = parse_yaml(prefixFile)
            # True indicates from preprocessed folder created during preprocessing step
            # Turn False if source should be from ssot/yaml/
            iterate_folder(get_yaml_location(item), item)

        logger.info("<=== Successfully generated schemas")
