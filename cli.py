"""
Module Name: Cli.

Script to generate Ontology and Shapes files.

"""
import argparse
import csv
import logging
import subprocess
from pathlib import Path

import colorlog

import yaml
import yaml2ttl

# configure logger
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
formatter = colorlog.ColoredFormatter(
    '%(asctime)s - %(log_color)s%(levelname)s - %(message)s%(reset)s',
    datefmt='%Y-%m-%d %H:%M:%S',
    log_colors={
        # 'DEBUG': 'blue',
        # 'INFO': 'green',
        'WARNING': 'yellow',
        'ERROR': 'red',
        'CRITICAL': 'bold_red',
    }
)
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
logger.addHandler(stream_handler)

# Command Line Interface for the script
if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        description='generates ontology files based single-source-of-truth')
    sub_parser = parser.add_subparsers(title="Output to be generated", dest="output")

    # Setup parser for sd-attribute command
    y2j_parser = sub_parser.add_parser("ontology-generation",
                                       help="Creates a turtle file for each prefix in the single source of truth, which defines the ontology")

    y2j_parser.add_argument('--srcFile', type=str, required=True,
                            help='Path to CSV file specifying where to find specific files')
    y2j_parser.add_argument('--dstPathOnto', type=str, required=True,
                            help='Path to file  file specifying where to generating specific files')
    y2j_parser.add_argument('--dstPathSchema', type=str, required=True,
                            help='Path to file  file specifying where to generating specific files')
    y2j_parser.add_argument('-e', '--ecosystem', action='append', required=True,
                            help='Select ecosystem, if ecosystem depends on other ecosystem please specify as list e.g ( --ecosystem gax-core --ecosystem trusted-cloud')

    args = parser.parse_args()
    if args.output == "ontology-generation":

        src_path: str = args.srcFile
        ontology_target_folder = args.dstPathOnto
        schema_target_folder = args.dstPathSchema
        ecosystems: list = args.ecosystem

        with open(src_path, encoding='utf-8') as file:
            csvreader = csv.reader(file)
            dict_from_csv = {}
            for row in csvreader:
                dict_from_csv[row[0]] = row[1]

        # loading data
        root_path: str = dict_from_csv['SSOT_root_path']
        validation_path: str = dict_from_csv['SSOT_validation_path']

        # load all yaml files associated with specified ecosystem
        files = [path for path in yaml2ttl.load_yml_file_paths(Path(root_path), ecosystems=ecosystems)]

        # load all prefixes associated with ecosystem
        prefixes = [path for path in yaml2ttl.load_yml_file_paths(Path(validation_path), ecosystems=ecosystems) if
                    path.name == "prefixes.yaml"]

        prefixes_metadata = [path for path in yaml2ttl.load_yml_file_paths(Path(validation_path), ecosystems=ecosystems)
                             if path.name != "prefixes.yaml" and path.name != 'dataTypeAbbreviation.yaml']

        # sorting files per ecosystem
        files = yaml2ttl.sort_by_ecosystem(root=Path(root_path), files=files, ecosystems=ecosystems)
        prefixes = yaml2ttl.sort_by_ecosystem(root=Path(validation_path), files=prefixes, ecosystems=ecosystems)
        prefixes_metadata = yaml2ttl.sort_by_ecosystem(root=Path(validation_path), files=prefixes_metadata,
                                                       ecosystems=ecosystems)

        # finding files associated to defined prefix of ecosystem
        matches = {i: {} for i in ecosystems}
        for eco in ecosystems:
            assert len(prefixes[eco]) == 1
            prefix_dict = yaml2ttl.parse_yaml(str(prefixes[eco][0]))
            found_matches = yaml2ttl.find_matches(files=files[eco], prefixes=prefix_dict, ecosystem=eco)
            matches[eco].update(found_matches)

        # preparing metadata files per prefix and ecosystem
        for k in prefixes_metadata.keys():
            prefixes_metadata[k] = {str(i.name).split('.')[0]: i for i in prefixes_metadata[k]}

        logger.info("Starting to generate ontology files (*.ttl) ===>")

        for eco in ecosystems:
            for prefix_name in matches[eco].keys():

                # Loading simple datatypes
                simple_data_types = []
                data_type_path = None
                try:
                    data_type_path = Path(validation_path) / eco / 'dataTypeAbbreviation.yaml'
                    with open(data_type_path, 'r') as file:
                        # Attempt to load the YAML file and extract its keys
                        data = yaml.safe_load(file)
                        if data is not None:
                            simple_data_types = list(data.keys())
                except FileNotFoundError:
                    # Handle the case where the file does not exist
                    logger.warning("%s not found.", data_type_path)
                except yaml.YAMLError as e:
                    # Handle YAML parsing errors
                    logger.error("Error parsing YAML file %s: %s", data_type_path, e)
                except AttributeError:
                    # Handle cases where the file is empty or its content is invalid
                    logger.warning("%s is empty or invalid.", data_type_path)
                except Exception as e:
                    # Handle unexpected errors
                    logger.error("An unexpected error occurred while processing %s: %s", data_type_path, e)

                assert len(prefixes[eco]) == 1
                eco_prefix = yaml2ttl.parse_yaml(str(prefixes[eco][0]))
                prefix_metadata = yaml2ttl.parse_yaml(str(prefixes_metadata[eco][prefix_name]))

                yaml2ttl.generate_ontology_file(
                    save_to=Path(ontology_target_folder),
                    name=prefix_name,
                    yaml_base_files=matches[eco][prefix_name],
                    prefixes=eco_prefix,
                    simple_data_types=simple_data_types,
                    prefix_metadata_dic=prefix_metadata,
                    ecosystems=ecosystems
                )

        logger.info("<=== Successfully generated ontology")

        for eco in ecosystems:
            cmdGeneratedSchema = [
                "python",
                "yaml2shacl.py",
                "shape-generation",
                "--dstPath", schema_target_folder,
                "-e", eco
            ]
            try:
                subprocess.run(cmdGeneratedSchema, check=True)
            except subprocess.CalledProcessError as exc:
                logger.error("Error occurred while running command %s", str(exc))
                raise exc
