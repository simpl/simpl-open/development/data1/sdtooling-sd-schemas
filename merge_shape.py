"""
Module Name: Merge Shape.

This module provides utility functions for merging shape files.

"""

from rdflib import Graph
import os
import argparse
import logging
import colorlog

# configure logger
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
formatter = colorlog.ColoredFormatter(
    '%(asctime)s - %(log_color)s%(levelname)s - %(message)s%(reset)s',
    datefmt='%Y-%m-%d %H:%M:%S',
    log_colors={
        # 'DEBUG': 'blue',
        # 'INFO': 'green',
        'WARNING': 'yellow',
        'ERROR': 'red',
        'CRITICAL': 'bold_red',
    }
)
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
logger.addHandler(stream_handler)

def merge_shape(ont1, ont2, output):
    """
    Merge two ontology files in Turtle format into a single output file.

    Args:
        ont1 (str): Path to the first shape file in Turtle format.
        ont2 (str): Path to the second shape file in Turtle format.
        output (str): Path to the output file where the merged shape will be saved.

    Returns:
        None

    Raises:
        rdflib.plugin.PluginException: If there are issues with parsing the Turtle files.
        IOError: If there are issues with file reading or writing.

    Example:
        merge_shape("shape1.ttl", "shape2.ttl", " merged-shapes.ttl")
    """
    g = Graph()
    g.parse(ont1, format='turtle')
    g.parse(ont2, format='turtle')
    g.serialize(output, format='turtle')
    
# Command Line Interface for the script
if __name__ == '__main__':

    outputFile = "merged-shapes.ttl"

    parser = argparse.ArgumentParser(
        description='generates merged files based single-source-of-truth')
    sub_parser = parser.add_subparsers(title="Output to be generated", dest="output")

    # Setup parser for sd-attribute command
    y2j_parser = sub_parser.add_parser("merged-generation",
                                       help="Creates a turtle file merged from shapes")

    y2j_parser.add_argument('files', metavar='F', type=str, nargs='+', help='List of files to process')

    args = parser.parse_args()

    if args.output == "merged-generation":
        files = args.files

        try:
            # Check if the list has at least 2 files
            if len(files) < 2:
                raise ValueError("The list must contain at least 2 files.")

            # Process the first two files
            merge_shape(files[0], files[1], outputFile)
            logger.info("Processing file: %s", str(files[0]))
            logger.info("Processing file: %s", str(files[1]))

            # Process the files starting from the third
            for index in range(2, len(files)):
                merge_shape(files[index], outputFile, outputFile)
                logger.info("Processing file: %s", str(files[index]))

            logger.info("Output file: %s", str(outputFile))

        except ValueError as ve:
            logger.error("Value error occurred: %s", str(ve))
        except Exception as e:
            logger.error("An error occurred: %s", str(e))
            raise e