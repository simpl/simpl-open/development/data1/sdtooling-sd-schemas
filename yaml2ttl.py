"""
Module Name: Yaml to Ttl.

This module provides utility functions for converting yaml to ttl files.
It includes functions for loading, processing, and saving files.

"""

# !/usr/bin/env python3

import logging
import colorlog
import os
import re
from datetime import datetime
from pathlib import Path
from typing import Dict, List

import pytz
from colorama import Fore, Style

import yaml

# configure logger
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
formatter = colorlog.ColoredFormatter(
    '%(asctime)s - %(log_color)s%(levelname)s - %(message)s%(reset)s',
    datefmt='%Y-%m-%d %H:%M:%S',
    log_colors={
        # 'DEBUG': 'blue',
        # 'INFO': 'green',
        'WARNING': 'yellow',
        'ERROR': 'red',
        'CRITICAL': 'bold_red',
    }
)
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
logger.addHandler(stream_handler)

# configure constants
TIMEZONE = 'Europe/Paris'
ENCODING = 'utf-8'


def parse_yaml(path) -> Dict:
    """
    Parse a YAML file and return its contents as a dictionary.

    Args:
        path (str): Path to the YAML file.

    Returns:
        Dict: Parsed data from the YAML file.

    Raises:
        yaml.YAMLError: If the YAML file contains a syntax error.
    """
    with open(path, "r") as stream:
        try:
            data = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            logger.error("Parse yaml error %s", str(exc))
            raise exc
    return data


def load_yml_file_paths(yml_folder: Path, ecosystems: List) -> List:
    """
    Load YAML file paths from a specified folder and its subdirectories.

    This function iterates through a directory structure and collects the paths
    of files with the `.yaml` extension, filtering them based on the provided
    ecosystems list. Files with the `.yml` extension are ignored and logged as warnings.

    Args:
        yml_folder (Path): The root folder containing YAML files.
        ecosystems (List): A list of ecosystem names used to filter directories.

    Returns:
        List: A list of paths to `.yaml` files that match the specified criteria.

    Logs:
        - Info: When the loading process starts.
        - Debug: The directory structure being processed.
        - Warning: When files with `.yml` extensions are encountered.
    """
    file_paths = []
    eco_sys: list = [str(yml_folder / str(eco)) for eco in ecosystems]
    logger.info("Loading yaml file...")
    for root, dirs, files in os.walk(str(yml_folder)):
        if root.startswith(tuple(eco_sys)):
            path = root.split(os.sep)
            logger.debug("%s%s", (len(path) - 1) * '---', os.path.basename(root))
            for file in files:
                if file.endswith(".yaml"):
                    file_paths.append(Path(root + "/" + file))
                elif file.endswith(".yml"):
                    logger.warning("Warning file %s -- not considered. Rename to extension .yaml",
                                   str(root + "/" + file))
    logger.info("Loading complete")
    return file_paths


def sort_by_ecosystem(root: Path, files: list, ecosystems: list) -> dict:
    """
    Classify files by their corresponding ecosystem.

    This function organizes a list of file paths into a dictionary,
    grouping them based on the ecosystems they belong to. Each ecosystem
    is identified by its subdirectory under the specified root.

    Args:
        root (Path): The root directory containing ecosystem subdirectories.
        files (list): A list of file paths to classify.
        ecosystems (list): A list of ecosystem names used to group files.

    Returns:
        dict: A dictionary where keys are ecosystem names and values are lists
        of file paths belonging to each ecosystem.

    Example:
        If `root` is `/data`, `files` contains paths like `/data/eco1/file1.yaml`,
        and `ecosystems` includes `eco1`, the result will be:
        {'eco1': ['/data/eco1/file1.yaml'], ...}
    """
    # setup empty dictionary
    classified_by_ecosystem = {i: [] for i in ecosystems}

    for path in files:
        for eco in ecosystems:
            eco_path = str(root / eco)
            if str(path).startswith(eco_path):
                classified_by_ecosystem[eco].append(path)

    return classified_by_ecosystem


def string_escape(s, encoding=ENCODING):
    r"""
    Escape and decode a string by using multiple encodings.

    This function first encodes the input string into `latin1` bytes, then decodes it
    using the 'unicode-escape' codec to perform octal-escaping. It then re-encodes
    the string back to bytes using `latin1` and finally decodes it into the original
    encoding (default is UTF-8).

    Args:
        s (str): The input string to be escaped and decoded.
        encoding (str, optional): The encoding to use for the final decoding (default is 'utf-8').

    Returns:
        str: The decoded string after escaping and re-decoding through multiple encodings.

    Example:
        string_escape('Hello\\nWorld')  # Would return 'Hello\nWorld' with proper escape handling.
    """
    return (s.encode('latin1')  # To bytes, required by 'unicode-escape'
            .decode('unicode-escape')  # Perform the actual octal-escaping decode
            .encode('latin1')  # 1:1 mapping back to bytes
            .decode(encoding))  # Decode original encoding


def camel_case_split(string):
    """
    Split a camel case string into separate words.

    This function takes a camel case string (e.g., 'camelCaseString') and splits it into individual words,
    inserting spaces between them. The function uses a regular expression to identify camel case patterns
    and separates them accordingly.

    Args:
        string (str): The input string in camel case to be split.

    Returns:
        str: The input string split into individual words, with spaces between them.

    Example:
        camel_case_split('camelCaseString')  # Returns 'camel Case String'
    """
    strings = re.findall(r'[A-Za-z](?:[a-z]+|[A-Z]*(?=[AZ]|$))', string)

    result = ""
    for i, itm in enumerate(strings):
        result = result + itm
        if i + 1 < len(strings):
            result = result + " "

    return result


def generate_ontology_file(
        save_to: Path,
        name: str,
        yaml_base_files: List,
        prefixes: dict,
        simple_data_types: List = None,
        prefix_metadata_dic: dict = None,
        ecosystems: List = None
):
    """
    Generate a TTL (Turtle) ontology file based on provided YAML files and saves it to the specified directory.

    This function processes a list of YAML files associated with a given prefix and generates an ontology
    in the Turtle format, which describes classes, properties, and metadata. The ontology file is saved
    in the provided destination directory with a file name based on the given prefix name.

    It also handles prefix definitions, class descriptions, property definitions, and ensures that
    specific properties are classified correctly based on data types (simple data types are distinguished from object properties).

    Args:
        save_to (Path): The path to the folder where the generated TTL file will be saved.
        name (str): The name of the prefix used to name the generated TTL file.
        yaml_base_files (List): A list of paths to YAML files that define ontology classes and properties.
        simple_data_types (List): A list of simple data types that distinguish between `owl:DatatypeProperty` and `owl:ObjectProperty`.
        prefixes (dict): A dictionary containing the prefix definitions to be added to the TTL file.
        prefix_metadata_dic (dict): A dictionary containing metadata for the ontology, including descriptions of prefixes.
        ecosystems (List): A list of ecosystems that help categorize which attributes to include in the ontology.

    Returns:
        None: This function generates and saves a TTL file to the specified `save_to` path.

    Example:
        generate_ontology_file(Path('/path/to/save'), 'examplePrefix', [Path('file1.yaml'), Path('file2.yaml')],
                               ['string', 'integer'], {'Prefixes': [...]}, {'OntologyDescription': [...]}, ['ecosystem1', 'ecosystem2'])

    Notes:
        - The function excludes any files from the ontology if they reside within a 'contract' directory.
        - All classes and properties are written with proper metadata, including labels, descriptions, and domains/ranges.
        - The function supports simple error handling and skipping certain files based on ecosystem information.
    """
    if simple_data_types is None:
        simple_data_types = []
    if prefix_metadata_dic is None:
        prefix_metadata_dic = {}
    if ecosystems is None:
        ecosystems = []

    ## distance between name and value.
    distance = "\t\t"

    file_name = str(name) + "_ontology_generated.ttl"
    # parse list of Paths to list of filenames without full path
    # base_files = [x.name for x in yaml_base_files]
    logger.info("Generating %s", str(save_to / file_name))

    ttl_file = open((save_to / file_name), "w")

    #### prefix definition
    for prefix in prefixes["Prefixes"]:
        line = "@prefix %s: <%s> .\n" % (prefix["name"], prefix["value"])
        ttl_file.write(line)

    end_of_line = "\n\n"
    ttl_file.write(end_of_line)

    # generate prefix content
    line = name + ": \n"
    ttl_file.write(line)
    for itm in prefix_metadata_dic['OntologyDescription']:
        itm_name = itm['name']
        value = itm['value']
        if isinstance(value, list):
            value = ', '.join(value)

        line = itm_name + distance + value + " ;\n"
        ttl_file.write(line)

    today = datetime.now(pytz.timezone(TIMEZONE)).isoformat(' ', 'seconds').replace(" ", "T")
    line = 'dct:modified' + distance + "\"" + today + "\"^^xsd:dateTimeStamp ;\n"

    ttl_file.write(line)

    end_of_line = ".\n\n"
    ttl_file.write(end_of_line)

    ## END of prefix context ##

    # generate yaml file content
    for file in yaml_base_files:

        # ENG Exclude contract dir from ontology
        file_path = Path(file)
        if "contract" in [part.name for part in file_path.parents]:
            logger.warning("Contract directory found. File excluded from ontology -> %s", str(file))
            continue

        logger.info("Considering -> %s",
                    str(file))

        yml_dic = parse_yaml(file)
        for key in [*yml_dic.keys()]:
            class_name = key
            prefix = yml_dic[key]["prefix"]
            subclasses = yml_dic[key]["subClassOf"]
            assert prefix == name
            full_class_name = prefix + ":" + class_name

            ttl_file.write('################## \n##{0}\n##################\n\n'.format(class_name))

            ttl_file.write(full_class_name)

            l0 = "\n\ta{0} owl:Class;".format(distance)
            l1 = "\n\trdfs:label{0} \"{1}\"@en ;".format(distance, camel_case_split(class_name))

            l2 = ""
            if subclasses:
                l2 = "\n\trdfs:subClassOf{0}".format(distance)

                subclasses_modified = []
                for sub_class in subclasses:
                    if str(sub_class).startswith("http"):
                        subclasses_modified.append("<" + sub_class + ">")
                    else:
                        subclasses_modified.append(sub_class)

                l2 = l2 + ', '.join(subclasses_modified)
                l2 = l2 + " ;"

            line_list = [l0, l1, l2]
            for itm in line_list:
                ttl_file.write(itm)

            end_of_line = "\n. \n\n"
            ttl_file.write(end_of_line)

            for attribute in yml_dic[key]["attributes"]:

                title = attribute['title']
                prefix = attribute['prefix']
                data_type = attribute['dataType']
                # removes any quotes in string
                description = attribute['description'].replace('"', '\\"')

                indicator = True
                for ecosystem in ecosystems:
                    if ecosystem in prefix:
                        indicator = False
                        li0 = prefix + ":" + title
                        if data_type in simple_data_types:
                            li1 = "\n\ta{0} owl:DatatypeProperty ;".format(distance)
                        else:
                            li1 = "\n\ta{0} owl:ObjectProperty ;".format(distance)

                        li2 = "\n\trdfs:label{0} \"{1}\"@en ;".format(distance, camel_case_split(title).lower())
                        li3 = "\n\trdfs:domain{0} {1} ;".format(distance, full_class_name)
                        li4 = "\n\trdfs:range{0} {1} ;".format(distance, data_type)
                        li5 = "\n\trdfs:comment{0} \"{1}\" ;".format(distance, description)

                        line_list = [li0, li1, li2, li3, li4, li5]
                        for itm in line_list:
                            ttl_file.write(itm)

                        end_of_line = "\n. \n\n"
                        ttl_file.write(end_of_line)
                if indicator:
                    pass
                    logger.info("Escaping: " + prefix + ":" + title)

    ttl_file.close()


def find_matches(files: list, prefixes: Dict, ecosystem: str) -> Dict:
    """
    Find and returns a dictionary of files that are associated with the given ecosystem prefix.

    This function processes a list of YAML files, extracts their prefixes, and associates them with
    the corresponding ecosystem prefix. It creates a dictionary where the keys are the prefixes
    associated with the ecosystem, and the values are lists of files that match those prefixes.
    Files that do not match any prefix from the ecosystem are ignored, and a warning message is printed.

    Args:
        files (list): A list of paths to YAML files to be processed.
        prefixes (Dict): A dictionary containing prefix definitions for the ecosystem.
        ecosystem (str): The ecosystem used to filter and find matching prefixes.

    Returns:
        Dict: A dictionary where the keys are ecosystem prefixes, and the values are lists of matching files.

    Example:
        find_matches([Path('file1.yaml'), Path('file2.yaml')], {'Prefixes': [...]}, 'ecosystem1')

    Notes:
        - Files that do not match any ecosystem prefix are ignored, and a warning is printed.
        - If a prefix is not found in the provided ecosystem, it is excluded from the result.
    """
    # creates output dictionary with expected prefixes
    keys = [p['name'] for p in prefixes["Prefixes"] if ecosystem in p['name']]
    matches = dict(zip(keys, [None] * len(keys)))

    # get prefix of yaml file
    for file in files:
        yaml_dic = parse_yaml(file)

        name = list(yaml_dic.keys())
        prefix = yaml_dic[name[0]]['prefix']

        if prefix in keys:
            if matches[prefix] is None:
                matches[prefix] = [file]
            else:
                matches[prefix].append(file)
        else:
            logger.warning(Fore.YELLOW + str(file) + " uses " + str(prefix) + " current ecosystem is " + str(
                ecosystem) + " => file is ignored during ontology generation " + Style.RESET_ALL)  # the path that was originally here was a lie

    # remove prefixes that are not found
    for itm in list(matches.keys()):
        if matches[itm] is None:
            matches.pop(itm, None)

    return matches
