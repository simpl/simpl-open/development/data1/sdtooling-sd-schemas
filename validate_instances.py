"""
Module Name: Validation instances.

Script to validate all Self-Description instances against a set of shacl shapes.

Call script with two parameters.
- First parameter: directory with SD instances as json-ld files. All json files will be considered.
- Second parameter: directory with shacl shapes. All shacl shapes will be considered.

"""

import logging
import os
import sys
from typing import List

import colorlog
from pyshacl import validate
from rdflib import Graph

# configure logger
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
formatter = colorlog.ColoredFormatter(
    '%(asctime)s - %(log_color)s%(levelname)s - %(message)s%(reset)s',
    datefmt='%Y-%m-%d %H:%M:%S',
    log_colors={
        # 'DEBUG': 'blue',
        # 'INFO': 'green',
        'WARNING': 'yellow',
        'ERROR': 'red',
        'CRITICAL': 'bold_red',
    }
)
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
logger.addHandler(stream_handler)

def get_all_files(dir: str, file_extension: str) -> List[str]:
    """
    Recursively retrieve all files with a specific extension from a directory.

    This function traverses the given directory and its subdirectories to find
    and return all files that have the specified file extension.

    Args:
        dir (str): The path to the directory to search.
        file_extension (str): The file extension to filter by (e.g., ".json", ".ttl").

    Returns:
        List[str]: A list of absolute file paths matching the specified extension.

    Example:
        >>> get_all_files("/path/to/dir", ".json")
        ['/path/to/dir/file1.json', '/path/to/dir/subdir/file2.json']

    Note:
        - This function uses recursion to search subdirectories.
        - The returned file paths are absolute paths.
    """
    files = []

    for file in os.listdir(os.path.abspath(dir)):
        path = os.path.join(dir, file)
        if os.path.isdir(path):
            files.extend(get_all_files(path, file_extension))
        else:
            if path.endswith(file_extension):
                files.append(path)
    return files


if __name__ == '__main__':
    # Input validation
    if len(sys.argv) != 3:
        logger.error("Usage: python validate_instances.py <instances_dir> <shapes_dir>")
        sys.exit(1)

    success = True
    instances_dir = sys.argv[1]
    shapes_dir = sys.argv[2]

    # Ensure directories exist
    if not os.path.isdir(instances_dir):
        logger.error("Instances directory '%s' does not exist.", str(instances_dir))
        sys.exit(1)

    if not os.path.isdir(shapes_dir):
        logger.error("Shapes directory '%s' does not exist.", str(shapes_dir))
        sys.exit(1)

    # Load and parse files
    instances = {
        ins_file: Graph().parse(source=ins_file, format="json-ld")
        for ins_file in get_all_files(instances_dir, ".json")
    }
    shapes = {
        sh_file: Graph().parse(source=sh_file)
        for sh_file in get_all_files(shapes_dir, ".ttl")
    }

    logger.info("Validating instances...")

    # Validate each instance against all shapes
    for ins_file, ins_graph in instances.items():
        logger.info("Validating " + ins_file + "... ")
        cons_conforms = True
        cons_results_text = ""

        for sh_file, sh_graph in shapes.items():
            try:
                conforms, _, results_text = validate(Graph().parse(source=ins_file, format="json-ld"),
                                                     shacl_graph=Graph().parse(source=sh_file),
                                                     inference='rdfs',
                                                     advanced=True,
                                                     js=True,
                                                     meta_shacl=True)

                cons_conforms = cons_conforms and conforms
                cons_results_text += results_text

            except Exception as e:
                logger.error("Validation failed with error: %s", str(e))
                success = False
                break

        if cons_conforms:
            logger.info("Validation success")
        else:
            logger.info("Validation error: %s", str(cons_results_text))
            success = False

    # Exit with appropriate code
    sys.exit(0 if success else 1)
